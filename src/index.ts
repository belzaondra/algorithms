import { Book } from "./Model/Book";
import { Catalog } from "./Model/Catalog";
import { CD } from "./Model/CD";
import { Movie } from "./Model/Movie";

const catalog = new Catalog();

///////////////////////////
// Book
///////////////////////////
const historyOfEverything = new Book(
  "A Short History of Nearly Everything",
  "Bill Bryson",
  544
);

catalog.addBook(historyOfEverything);

//Changes status to true
historyOfEverything.toggleCheckOutStatus();

//logs status
console.log(historyOfEverything.isCheckedOut);

//adds new rating with value 5
historyOfEverything.addRating(5);

console.log(
  `Average rating of ${
    historyOfEverything.title
  } is ${historyOfEverything.getAverageRating()}.`
);

///////////////////////////
// Movie
///////////////////////////

const speed = new Movie("Speed", "Jan de Bont", 116);

catalog.addMovie(speed);
speed.toggleCheckOutStatus();

console.log(historyOfEverything.isCheckedOut);

const speedRatings = [1, 1, 5];
speedRatings.forEach((r) => speed.addRating(r));

console.log(`Average rating of ${speed.title} is ${speed.getAverageRating()}.`);

///////////////////////////
// CD
///////////////////////////

const clouds = new CD("Clouds", "NF", [
  "CLOUDS",
  "THAT'S A JOKE",
  "JUST LIKE YOU",
  "STORY",
  "PRIDEFUL",
  "LOST",
  "LAYERS",
  "DRIFTING",
  "TRUST",
  "PAID MY DUES",
  "CLOUDS (EDIT)",
]);

catalog.addCD(clouds);
clouds.toggleCheckOutStatus();

const cloudsRatings = [1, 1, 1, 1, 1, 1, 1, 1, 2];
cloudsRatings.forEach((r) => clouds.addRating(r));

console.log(clouds.shuffle());

// console.log(catalog);

//gets book from catalog
if (catalog.books.length) console.log(catalog.getBook(catalog.books[0].id));
