import { Media } from "./Media";

export class Movie extends Media {
  private _director: string;
  private _runtime: number;
  constructor(title: string, director: string, runtime: number) {
    super(title);
    this._director = director;
    this._runtime = runtime;
  }

  get director() {
    return this._director;
  }

  get runtime() {
    return this._runtime;
  }
}
