import { Media } from "./Media";

export class Book extends Media {
  private _author: string;
  private _pages: number;

  constructor(title: string, author: string, pages: number) {
    super(title);
    this._author = author;
    this._pages = pages;
  }

  get author() {
    return this._author;
  }

  get pages() {
    return this._pages;
  }
}
