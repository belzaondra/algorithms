import { getRandomNumber } from "../Utils/numberGeneratorUtils";
import { Media } from "./Media";
import _ from "lodash";

export class CD extends Media {
  private _artist: string;
  private _songs: string[];
  constructor(title: string, artist: string, songs: string[]) {
    super(title);
    this._artist = artist;
    this._songs = songs;
  }

  get artist() {
    return this._artist;
  }

  get songs() {
    return this._songs;
  }

  shuffle() {
    const shuffledSongs: string[] = [];
    let songsBackup = _.cloneDeep(this._songs);

    do {
      let num = getRandomNumber(0, songsBackup.length - 1);
      shuffledSongs.push(songsBackup[num]);
      _.pullAt(songsBackup, [num]);
    } while (songsBackup.length !== 0);

    return shuffledSongs;
  }
}
