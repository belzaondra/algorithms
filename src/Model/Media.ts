import { v4 as uuidv4 } from "uuid";

export class Media {
  private _title: string;
  private _id: string;
  private _isCheckedOut: boolean = false;
  private _ratings: number[] = [];
  constructor(title: string) {
    this._title = title;
    this._id = uuidv4();
  }

  get ratings() {
    return this._ratings;
  }

  get title() {
    return this._title;
  }

  get isCheckedOut() {
    return this._isCheckedOut;
  }

  get id() {
    return this._id;
  }

  set isCheckedOut(checkedOut: boolean) {
    this._isCheckedOut = checkedOut;
  }

  getAverageRating() {
    if (!this._ratings.length) return null;
    return (
      this._ratings.reduce((sum, cur) => (sum += cur), 0) / this._ratings.length
    );
  }

  toggleCheckOutStatus() {
    this._isCheckedOut = !this._isCheckedOut;
  }

  addRating(rating: number) {
    if (rating < 1 || rating > 5) throw "Invalid rating";
    this._ratings.push(rating);
  }
}
