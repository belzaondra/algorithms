import _ from "lodash";
import { Book } from "./Book";
import { CD } from "./CD";
import { Movie } from "./Movie";

export class Catalog {
  private _movies: Movie[] = [];
  private _cds: CD[] = [];
  private _books: Book[] = [];
  constructor() {}

  get movies() {
    return this._movies;
  }

  //Gets movie by Id
  getMovie(id: string) {
    return this._movies.find((m) => m.id === id);
  }

  get cds() {
    return this._cds;
  }

  //Gets cd by Id
  getCD(id: string) {
    return this._cds.find((cd) => cd.id === id);
  }

  get books() {
    return this._books;
  }

  //Gets book by Id
  getBook(id: string) {
    return this._books.find((b) => b.id === id);
  }

  // Adds new book
  addBook(book: Book) {
    this._books.push(book);
  }

  // Removes book by Id
  removeBook(id: string) {
    const index = this._books.findIndex((b) => b.id === id);
    if (index === -1) throw "Book not found";
    _.pullAt(this._books, [index]);
  }

  // Adds new cd
  addCD(cd: CD) {
    this._cds.push(cd);
  }

  // Removes CD by Id
  removeCD(id: string) {
    const index = this._cds.findIndex((b) => b.id === id);
    if (index === -1) throw "CD not found";
    _.pullAt(this._cds, [index]);
  }

  // Adds movie
  addMovie(movie: Movie) {
    this._movies.push(movie);
  }

  // Removes movie by Id
  removeMovie(id: string) {
    const index = this._movies.findIndex((b) => b.id === id);
    if (index === -1) throw "Movie not found";
    _.pullAt(this._movies, [index]);
  }
}
